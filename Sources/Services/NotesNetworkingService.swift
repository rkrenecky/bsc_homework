//
//  NotesNetworkingService.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Alamofire

final class NotesNetworkingService: NotesDataHandler {
    enum NotesError: Error {
        case decodingError
        case encodingError
    }

    struct Requests {
        static var getNotes: URLRequest {
            var request = URLRequest(url: Constants.API.Endpoints.getNotes)
            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
            request.httpMethod = HTTPMethod.get.rawValue

            return request
        }

        static func createNote(note: Note) -> URLRequest? {
            guard note.id == nil else {
                fatalError("The note cannot have assigned ID when trying to create it on the server!")
            }

            let encoder = JSONEncoder()
            guard let encodedBody = try? encoder.encode(note) else { return nil }

            var request = URLRequest(url: Constants.API.Endpoints.createNote)
            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
            request.httpMethod = HTTPMethod.post.rawValue
            request.httpBody = encodedBody

            return request
        }

        static func updateNote(note: Note) -> URLRequest? {
            guard let noteId = note.id else {
                fatalError("The note must have assigned ID when trying to update it on the server!")
            }

            let encoder = JSONEncoder()
            guard let encodedBody = try? encoder.encode(note) else { return nil }

            var request = URLRequest(url: Constants.API.Endpoints.updateNote(id: noteId))
            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
            request.httpMethod = HTTPMethod.put.rawValue
            request.httpBody = encodedBody

            return request
        }

        static func deleteNote(note: Note) -> URLRequest {
            guard let noteId = note.id else {
                fatalError("The note must have assigned ID when trying to delete it on the server!")
            }

            var request = URLRequest(url: Constants.API.Endpoints.deleteNote(id: noteId))
            request.allHTTPHeaderFields = ["Content-Type" : "application/json"]
            request.httpMethod = HTTPMethod.delete.rawValue

            return request
        }
    }

    private let session: NetworkingSession

    // Add dependency injection for mocking
    init(session: NetworkingSession = Alamofire.SessionManager.default) {
        self.session = session
    }

    func getNotes(completion: @escaping (Result<[Note]>) -> Void) {
        session.request(urlRequest: Requests.getNotes).validate().responseData { dataResponse in
            switch dataResponse.result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                do {
                    let decoder = JSONDecoder()
                    let notes = try decoder.decode([Note].self, from: data)

                    completion(.success(notes))
                } catch {
                    completion(.failure(NotesError.decodingError))
                }
            }
        }
    }

    func saveNote(note: Note, completion: @escaping (Result<Void>) -> Void) {
        let request: URLRequest
        if note.id == nil {
            guard let createRequest = Requests.createNote(note: note) else {
                completion(.failure(NotesError.encodingError))
                return
            }

            request = createRequest
        } else {
            guard let updateRequest = Requests.updateNote(note: note) else {
                completion(.failure(NotesError.encodingError))
                return
            }

            request = updateRequest
        }

        session.request(urlRequest: request).validate().responseData { dataResponse in
            switch dataResponse.result {
            case .failure(let error):
                completion(.failure(error))
            case .success:
                completion(.success(()))
            }
        }
    }

    func deleteNote(note: Note, completion: @escaping (Result<Void>) -> Void) {
        session.request(urlRequest: Requests.deleteNote(note: note)).validate().responseData { dataResponse in
            switch dataResponse.result {
            case .failure(let error):
                completion(.failure(error))
            case .success:
                completion(.success(()))
            }
        }
    }
}
