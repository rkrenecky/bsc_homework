//
//  Module.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

final class Module: ApplicationModule {
    let notesDataHandler: NotesDataHandler = NotesNetworkingService()
}
