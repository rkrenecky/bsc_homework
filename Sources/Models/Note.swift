//
//  Note.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct Note: Equatable, Codable {
    var id: Int?
    var title: String
}

extension Note {
    static let empty = Note(id: nil, title: "")
    static let sample = [
        Note(id: 0, title: "First note"),
        Note(id: 1, title: "Second note")
    ]
}
