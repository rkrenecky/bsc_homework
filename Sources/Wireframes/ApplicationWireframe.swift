//
//  ApplicationWireframe.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit

protocol ApplicationModule {
    var notesDataHandler: NotesDataHandler { get }
}

final class ApplicationWireframe {
    private let module: ApplicationModule
    private let rootNavigationController: UINavigationController

    init(module: ApplicationModule) {
        self.module = module
        self.rootNavigationController = UINavigationController()
    }

    func entrypoint() -> UIViewController {
        rootNavigationController.setViewControllers([notesListVC()], animated: false)
        return rootNavigationController
    }

    func notesListVC() -> NotesListViewController {
        let dependencies = NotesListViewController.Dependencies(dataHandler: module.notesDataHandler)
        let reactions = NotesListViewController.Reactions(
            addNoteSelected: {
                let noteDetailVC = UINavigationController(rootViewController: self.noteDetailVC(note: Note.empty))
                self.rootNavigationController.present(noteDetailVC, animated: true, completion: nil)
            },
            noteSelected: { note in
                let noteDetailVC = UINavigationController(rootViewController: self.noteDetailVC(note: note))
                self.rootNavigationController.present(noteDetailVC, animated: true, completion: nil)
            }
        )

        return NotesListViewController(dependencies: dependencies, reactions: reactions)
    }

    func noteDetailVC(note: Note) -> NoteDetailViewController {
        let dependencies = NoteDetailViewController.Dependencies(dataHandler: module.notesDataHandler)
        let reactions = NoteDetailViewController.Reactions(
            saveSuccessful: {
                self.rootNavigationController.dismiss(animated: true, completion: nil)
            },
            closeSelected: {
                self.rootNavigationController.dismiss(animated: true, completion: nil)
            }
        )

        return NoteDetailViewController(note: note, dependencies: dependencies, reactions: reactions)
    }
}
