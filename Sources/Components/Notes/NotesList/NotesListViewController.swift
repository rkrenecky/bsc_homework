//
//  NotesListViewController.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit
import SnapKit
import MBProgressHUD

final class NotesListViewController: UIViewController {
    private static let cellIdentifier = "NoteCell"

    struct Dependencies {
        let dataHandler: NotesDataHandler
    }
    struct Reactions {
        let addNoteSelected: () -> Void
        let noteSelected: (Note) -> Void
    }

    private let dependencies: Dependencies
    private let reactions: Reactions

    private var notes: [Note] = [] { didSet { tableView.reloadData() } }

    private let tableView = UITableView()
    private let activityIndicator = UIActivityIndicatorView()

    init(dependencies: Dependencies, reactions: Reactions) {
        self.dependencies = dependencies
        self.reactions = reactions
        super.init(nibName: nil, bundle: nil)

        title = NSLocalizedString("NotesTitle", comment: "")
        setupView()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: NotesListViewController.cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
    }

    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        [tableView, activityIndicator].forEach(view.addSubview)

        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        activityIndicator.snp.makeConstraints { make in
            make.center.equalTo(view.safeAreaLayoutGuide)
        }

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAction))
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editAction))
        view.backgroundColor = .white
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        Styles.tableView(tableView: tableView)
        Styles.activityIndicator(activityIndicator: activityIndicator)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.prefersLargeTitles = true

        notes = []
        loadData()
    }

    private func loadData() {
        func showAlert(with message: String?) {
            let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: ""), style: .default, handler: { _ in
                getNotes()
            }))

            present(alert, animated: true, completion: nil)
        }

        func getNotes() {
            activityIndicator.startAnimating()

            dependencies.dataHandler.getNotes() { [weak self] result in
                guard let strongSelf = self else { return }

                strongSelf.activityIndicator.stopAnimating()

                switch result {
                case .failure:
                    showAlert(with: NSLocalizedString("NetworkingError", comment: ""))
                case .success(let notes):
                    strongSelf.notes = notes
                }
            }
        }

        getNotes()
    }

    @objc
    private func addAction() {
        reactions.addNoteSelected()
    }

    @objc
    private func editAction() {
        tableView.setEditing(!tableView.isEditing, animated: true)
    }
}

extension NotesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NotesListViewController.cellIdentifier) else {
            fatalError("Dequeued reusable cell is of a wrong type!")
        }

        cell.textLabel?.text = notes[indexPath.row].title
        return cell
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

extension NotesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        reactions.noteSelected(notes[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        func showAlert(with message: String?) {
            let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: ""), style: .default, handler: { [weak self] _ in
                guard let strongSelf = self else { return }

                deleteNote(note: strongSelf.notes[indexPath.row])
            }))

            present(alert, animated: true, completion: nil)
        }

        func deleteNote(note: Note) {
            guard let navController = navigationController else {
                fatalError("NoteListViewController must be embedded into UINavigationController!")
            }

            let hud = MBProgressHUD.showAdded(to: navController.view, animated: true)
            hud.label.text = NSLocalizedString("Delete", comment: "")

            dependencies.dataHandler.deleteNote(note: note) { [weak self] result in
                guard let strongSelf = self else { return }

                hud.hide(animated: true)

                switch result {
                case .failure:
                    showAlert(with: NSLocalizedString("NetworkingError", comment: ""))
                case .success:
                    strongSelf.notes.remove(at: indexPath.row)
                }
            }
        }

        if editingStyle == .delete {
            deleteNote(note: notes[indexPath.row])
        }
    }
}

extension NotesListViewController {
    struct Styles {
        static func tableView(tableView: UITableView) {
            tableView.tableFooterView = UIView()
        }

        static func activityIndicator(activityIndicator: UIActivityIndicatorView) {
            activityIndicator.style = .gray
            activityIndicator.hidesWhenStopped = true
        }
    }
}
