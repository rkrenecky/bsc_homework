//
//  NoteDetailViewController.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import UIKit
import SnapKit
import MBProgressHUD

final class NoteDetailViewController: UIViewController {
    static let placeHolderText = NSLocalizedString("NoteDetailPlaceholder", comment: "")

    struct Dependencies {
        let dataHandler: NotesDataHandler
    }
    struct Reactions {
        let saveSuccessful: () -> Void
        let closeSelected: () -> Void
    }

    private var note: Note {
        didSet {
            navigationItem.rightBarButtonItem?.isEnabled = note.title != originalTitle && !note.title.isEmpty
        }
    }
    private let originalTitle: String

    private let dependencies: Dependencies
    private let reactions: Reactions

    private let textView = UITextView()

    init(note: Note, dependencies: Dependencies, reactions: Reactions) {
        self.note = note
        self.originalTitle = note.title
        self.dependencies = dependencies
        self.reactions = reactions
        super.init(nibName: nil, bundle: nil)

        title = NSLocalizedString("NotesDetailTitle", comment: "")
        textView.delegate = self
        setupView()
    }

    @available (*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        view.addSubview(textView)

        textView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaLayoutGuide).inset(24)
        }

        textView.layer.borderWidth = 0.5
        textView.layer.cornerRadius = 8
        textView.layer.borderColor = UIColor.gray.cgColor
        textView.clipsToBounds = true

        if note.title.isEmpty {
            Styles.emptyTextView(textView: textView)
        } else {
            textView.text = note.title
            Styles.nonEmptyTextView(textView: textView)
        }

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeSelected))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveSelected))
        navigationItem.rightBarButtonItem?.isEnabled = false

        view.backgroundColor = .white
    }

    @objc
    private func closeSelected() {
        reactions.closeSelected()
    }

    @objc
    private func saveSelected() {
        func showAlert(with message: String?) {
            let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: { _ in }))
            alert.addAction(UIAlertAction(title: NSLocalizedString("Retry", comment: ""), style: .default, handler: { _ in
                save()
            }))

            present(alert, animated: true, completion: nil)
        }
        
        func save() {
            guard let navController = navigationController else {
                fatalError("NoteDetailViewController must be embedded into UINavigationController!")
            }

            let hud = MBProgressHUD.showAdded(to: navController.view, animated: true)
            hud.label.text = NSLocalizedString("Save", comment: "")

            dependencies.dataHandler.saveNote(note: note, completion: { [weak self] result in
                guard let strongSelf = self else { return }

                hud.hide(animated: true)

                switch result {
                case .failure:
                    showAlert(with: NSLocalizedString("NetworkingError", comment: ""))
                case .success:
                    strongSelf.reactions.saveSuccessful()
                }
            })
        }

        save()
    }
}

extension NoteDetailViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == NoteDetailViewController.placeHolderText {
            textView.text = ""
            Styles.nonEmptyTextView(textView: textView)
        }
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            Styles.emptyTextView(textView: textView)
        }
    }

    func textViewDidChange(_ textView: UITextView) {
        note.title = textView.text
    }
}

extension NoteDetailViewController {
    struct Styles {
        static func emptyTextView(textView: UITextView) {
            textView.text = NoteDetailViewController.placeHolderText
            textView.textColor = .lightGray
        }

        static func nonEmptyTextView(textView: UITextView) {
            textView.textColor = .black
        }
    }
}
