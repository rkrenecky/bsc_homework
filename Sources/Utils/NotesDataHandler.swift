//
//  NotesDataHandler.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation
import enum Alamofire.Result

protocol NotesDataHandler {
    func getNotes(completion: @escaping (Result<[Note]>) -> Void)
    func saveNote(note: Note, completion: @escaping (Result<Void>) -> Void)
    func deleteNote(note: Note, completion: @escaping (Result<Void>) -> Void)
}
