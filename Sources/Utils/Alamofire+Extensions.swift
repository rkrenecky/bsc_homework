//
//  Alamofire+Extensions.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 30/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Alamofire

extension DataRequest: NetworkingRequest {
    func responseData(completionHandler: @escaping (DataResponse<Data>) -> Void) {
        responseData(queue: nil, completionHandler: completionHandler)
    }
}

extension SessionManager: NetworkingSession {
    func request(urlRequest: URLRequestConvertible) -> NetworkingRequest {
        return request(urlRequest)
    }
}
