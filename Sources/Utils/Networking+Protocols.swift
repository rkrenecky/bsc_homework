//
//  Networking+Protocols.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 30/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Alamofire

protocol NetworkingSession {
    func request(urlRequest: URLRequestConvertible) -> NetworkingRequest
}

protocol NetworkingRequest {
    func responseData(completionHandler: @escaping (DataResponse<Data>) -> Void)
    func validate() -> Self
}
