//
//  Constants.swift
//  BSC_homework
//
//  Created by Robin Krenecky on 27/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Foundation

struct Constants {
    struct API {
        struct Endpoints {
            private static let baseURLstring = "https://private-9aad-note10.apiary-mock.com/"

            static let getNotes = URL(string: "\(baseURLstring)/notes")!

            static let createNote = URL(string: "\(baseURLstring)/notes")!

            static func updateNote(id: Int) -> URL {
                return URL(string: "\(baseURLstring)/notes/\(id)")!
            }

            static func deleteNote(id: Int) -> URL {
                return URL(string: "\(baseURLstring)/notes/\(id)")!
            }
        }
    }
}
