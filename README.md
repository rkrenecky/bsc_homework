# **BSC Homework**
This sample app is used for management of personal notes.

### Build
The project is written in Swift 4.2 programming language and was built with Xcode 10.1. 

Application is ready to build and run after clone of the repository as it contains everything the app needs. Just make sure to open `BSC_homework.xcworkspace` instead of the project file when building the app.

### Libraries
The application uses [CocoaPods](https://cocoapods.org) dependency manager and these open source libraries: [Alamofire](https://github.com/Alamofire/Alamofire), [MBProgressHUD](https://github.com/jdg/MBProgressHUD), [SnapKit](https://github.com/SnapKit/SnapKit) and [IQKeyboardManagerSwift](https://github.com/hackiftekhar/IQKeyboardManager).

