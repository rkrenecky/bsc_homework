//
//  Networking+Mocks.swift
//  BSC_homeworkTests
//
//  Created by Robin Krenecky on 30/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import Alamofire
@testable import BSC_homework

enum MockError: Error {
    case error
}

final class NetworkingRequestMock: NetworkingRequest {
    enum MockType {
        case failedResponse
        case wrongData
        case success
    }

    private let type: MockType

    init(type: MockType) {
        self.type = type
    }

    func responseData(completionHandler: @escaping (DataResponse<Data>) -> Void) {
        switch type {
        case .failedResponse:
            completionHandler(DataResponse<Data>(request: nil, response: nil, data: nil, result: .failure(MockError.error)))
        case .wrongData:
            let wrongData = Data(bytes: [1, 2, 3, 4, 5])
            completionHandler(DataResponse<Data>(request: nil, response: nil, data: nil, result: .success(wrongData)))
        case .success:
            let correctData = try! JSONEncoder().encode(Note.sample)
            completionHandler(DataResponse<Data>(request: nil, response: nil, data: nil, result: .success(correctData)))
        }
    }

    func validate() -> Self {
        return self
    }
}

final class FailedResponseNetworkingSessionMock: NetworkingSession {
    func request(urlRequest: URLRequestConvertible) -> NetworkingRequest {
        return NetworkingRequestMock(type: .failedResponse)
    }
}

final class WrongDataNetworkingSessionMock: NetworkingSession {
    func request(urlRequest: URLRequestConvertible) -> NetworkingRequest {
        return NetworkingRequestMock(type: .wrongData)
    }
}

final class SuccessNetworkingSessionMock: NetworkingSession {
    func request(urlRequest: URLRequestConvertible) -> NetworkingRequest {
        return NetworkingRequestMock(type: .success)
    }
}
