//
//  Test_NotesNetworkingService.swift
//  BSC_homeworkTests
//
//  Created by Robin Krenecky on 30/01/2019.
//  Copyright © 2019 BPA. All rights reserved.
//

import XCTest
import enum Alamofire.Result
@testable import BSC_homework

class Test_NotesNetworkingService: XCTestCase {
    func testFailingResponse() {
        let session = FailedResponseNetworkingSessionMock()
        let service = NotesNetworkingService(session: session)

        var result: Result<[Note]>?
        service.getNotes { result = $0 }

        XCTAssertTrue(result?.isFailure ?? false)

        guard let error = result?.error else {
            XCTFail()
            return
        }

        if case MockError.error = error {
            XCTAssertTrue(true)
        } else {
            XCTFail()
        }
    }

    func testWrongResponse() {
        let session = WrongDataNetworkingSessionMock()
        let service = NotesNetworkingService(session: session)

        var result: Result<[Note]>?
        service.getNotes { result = $0 }

        XCTAssertTrue(result?.isFailure ?? false)

        guard let error = result?.error else {
            XCTFail()
            return
        }

        if case NotesNetworkingService.NotesError.decodingError = error {
            XCTAssertTrue(true)
        } else {
            XCTFail()
        }
    }

    func testSuccessfulResponse() {
        let session = SuccessNetworkingSessionMock()
        let service = NotesNetworkingService(session: session)

        var result: Result<[Note]>?
        service.getNotes { result = $0 }

        XCTAssertTrue(result?.isSuccess ?? false)
        XCTAssertEqual(Note.sample, result?.value)
    }
}
